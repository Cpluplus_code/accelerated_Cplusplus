#ifndef GUARD_median_h
#define GUARD_median_h

// median.h
// declaration for median function 
#include <vector>
double median(std::vector<double>);

#endif
