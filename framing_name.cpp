// ask for a person's name, generate a framed greeting
#include <iostream>
#include <string>

int main()
{
  std::cout<<"Please enter first name:";
  std::string name;
  std::cin>>name;

  //build message to write
  const std::string greeting = "Hello, " + name + "!";

  // build second and fourth lines
  const std::string spaces(greeting.size(),' ');
  const std::string second = "* " + spaces + " *";

  // build 1st and 5th lines 
  const std::string first(second.size(), '*');

  // write it all
  std::cout << std::endl;
  std::cout <<first<<std::endl;
  std::cout<<second<<std::endl;
  std::cout<<"* "<<greeting<<" *"<<std::endl;
  std::cout<<second<<std::endl;
  std::cout<<first<<std::endl;

  return 0;
}

