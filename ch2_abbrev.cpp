// chapter 2 example program
#include <iostream>
#include <string>

using std::cout; // using declaration
using std::cin;
using std::endl;
using std::string;

int main()
{
  cout<<"Please enter first name: ";
  string name;
  cin>>name;
  
  // request frame padding
  cout<<"Please enter top padding: ";
  int top_pad;
  cin>>top_pad;
  cout<<"Please enter bottom padding: ";
  int bottom_pad;
  cin>>bottom_pad;
  cout<<"Please enter side padding: ";
  int side_pad;
  cin>>side_pad;

  //build message to write
  const string greeting = "Hello, " + name + "!";

  // number of blanks surrounding the greeting
  // const int side_pad = 1;
  // const int top_pad = 2;
  // const int bottom_pad = 1;

  // total number of rows to write
  const int rows = top_pad + bottom_pad + 3;
  const string::size_type cols = greeting.size() + side_pad*2+2;

  // separate output from input
  cout << endl;

  // write rows of output
  for (int r=0; r!=rows; ++r){
    string::size_type c = 0;

    while (c!=cols){
		  // write a row of output
		  if (r==top_pad+1 && c==side_pad+1){
		    cout<<greeting;
			c+=greeting.size();
		  }
		  else{
		    // are we on the border?
			if (r==0 || r==rows -1 || c==0 || c==cols-1)
			  cout<<"*";
			else
			  cout<<" ";
			++c;
		  }
	}

	cout<<endl;
  }
  return 0;
}

