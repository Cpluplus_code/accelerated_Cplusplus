#include <cmath>
#include <iomanip>  // setd::setprecision
#include <iostream> // std::cout, std::fixed

using namespace std;
using std::setprecision;

int get_width(int n) 
{
	return log10(n) + 1; // width of number + 1
}

int get_width(double n) 
{
	return log10(n) + 1; // width of number + 1
}

// istream& read_words(istream& in, vector<string>& words)
// {
// 	if (in) 
// 	{
// 		words.clear();
// 		string word;
// 
// 		while (in >> word)
// 			words.push_back(word);
// 		in.clear();
// 	}
// 	return in;
// }

int main()
{
	double maxNum = 1000.0;
	int extraWidth = 2;
	for (double i = 1.08; i<=maxNum; ++i)
		cout << setprecision(get_width(maxNum)+extraWidth) << setw(get_width(maxNum)+extraWidth) << i 
				 << setprecision(get_width(maxNum*maxNum)+extraWidth) << setw(get_width(maxNum*maxNum)+2*extraWidth) << i*i 
				 << endl;	

	return 0;
}


