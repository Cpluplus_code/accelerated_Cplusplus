class Widget {
public:
  Widget(Widget&& rhs); // rhs is an lvalue, though
  ...			// it has an rvalue ref type
};


