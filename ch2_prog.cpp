// chapter 2 example program
#include <iostream>
#include <string>

using std::cout; // using declaration

int main()
{
  std::cout<<"Please enter first name:";
  std::string name;
  std::cin>>name;

  //build message to write
  const std::string greeting = "Hello, " + name + "!";

  // number of blanks surrounding the greeting
  const int pad = 1;

  // total number of rows to write
  const int rows = pad*2 + 3;

  // separate output from input
  std::cout << std::endl;

  // write rows of output
  int r = 0;

  // invariant: we have written r rows so far
  while (r!=rows){
		  // write a row of output
		  std::cout<<std::endl; // loop invariant
		  const std::string::size_type cols = greeting.size() + pad*2 + 2;
		  ++r;
  }

 // for (init statement, condition, expression) for header
 //   statement for body

  std::string:;size_type c = 0;
  // invariant: we ahve written c chars so far in the current row
  while (c!=cols) {
	if (r==0 || r==rows-1 || c==0 || c==cols-1){
			std::cout<<"*";
	} else{
		  // write one or more nonborder chars
		  // adjust the value of c to maintain the invariant
  }

  return 0;
}

