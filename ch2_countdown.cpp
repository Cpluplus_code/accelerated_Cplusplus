#include <iostream>
#include <string>

int main()
{
  for (int i=10; i!=-6; --i){
	std::cout<<i<<std::endl;
  }

  // generate product of numbers in range [1,10)
  int product = 1;
  for (int i=1; i!=10; ++i){
		  product *= i;
  }
  std::cout<<product<<std::endl;
  
  std::cout<<"Enter 1st number: ";
  int num1;
  std::cin>>num1;
  std::cout<<"Enter 2nd number: ";
  int num2;
  std::cin>>num2;
  const int largest_num = (num1>num2)?num1:num2;
//  const std::string number = largest_num;
  std::cout<<largest_num<<std::endl;
  return 0;
}
