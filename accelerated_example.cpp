// small C++ program
#include <iostream>

int main()

{
  std::cout << "Hello, world!" << std::endl;
  return 0;
}


// std::cout operand has type std::ostream
// << is left associative
// ; discards value, interested only in sidefx
// scope: namespace (eg. std) :: is scope operator
// body of main, and other fxns is a scope
// two main types: built-in such as int, outside eg. std
// std lib defines names in headers using #include
// main can omit return, 0 implicit
//
