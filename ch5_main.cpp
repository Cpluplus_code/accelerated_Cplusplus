// chapter 5 : using sequential containers and analyzing strings

// predicate to determine wheterh a student failed
bool fgrade(const Student_info& s)
{
	return grade(s) < 60; 
}

// separate passing and failing student records
vector<Student_info> extract_fails(vector<Student_info>& students)
{
	vector<Student_info> pass, fail;

	for (vector<Student_info>::size_type i=0; i != students.size(); ++i)
		if (fgrade(students[i]))
			fail.push_back(students[i]);
		else
			pass.push_back(students[i]);

	students = pass; // overwrite students vector
	return fail; 
}

// 2nd solution: but slows exponentially with num of records
vector<Student_info> extract_fails(vector<Student_info>& students)
{
	vector<Student_info> fail;
	vector<Student_info>::size_type i = 0; // init loop variable

	// invariant: elements [0,i) of students represent passing grades
	while (i != students.size())
	{
		if (fgrade(students[i]))
		{
			fail.push_back(students[i]);
			students.erase(students.begin() + i);
		}
		else
			++i; // only increment if no element deletion
	}
	return fail;
}


// standard containers define two iterator types
// const_iterator and iterator
// * deref operator on iterator returns lvalue equiv. iter->name

for (vector<Student_info>::const_iterator iter = students.begin();
		iter != students.end(); ++iter)
{
	cout << iter->name << endl;
}

// version 3: iterators with no indexing; still kinda slow
vector<Student_info> extract_fails(vector<Student_info>& students)
{
	vector<Student_info> fail;
	vector<Student_info>::iterator iter = students.begin();
	while (iter != students.end())
	{
		if (fgrade(*iter))
		{
			fail.push_back(*iter);
			iter = students.erase(iter);
		}
		else
			++iter;
	}
	return fail;
}

// <list> optimized for fast insertion and deletion
// version 4: use list instead of vector
list<Student_info> extract_fails(list<Student_info>& students)
{
	list<Student_info> fail;
	list<Student_info>::iterator iter = students.begin();

	while (iter != students.end())
	{
		if (fgrade(*iter)) 
		{
			fail.push_back(*iter);
			iter = students.erase(iter);
		}
		else
			++iter;
	}
	return fail;
}


