// delimit words in string using indices 

vector<string> split(const string& s)
{
	vector<string> ret;
	typedef string::size_type string_size;
	string_size i = 0;

	// invariant: we have processed characters [original value of i, i) 
	while (i != s.size()) 
	{
		// ignore leading blanks
		// invariant: characters in range [original i, current i) are all spaces
		while (i != s.size() && isspace(s[i]))
			++i;

		// find end of next word
		string_size = j = i; 
		// invariant: none of the characters in range [original j, current j) is a
		// space
		while (j != s.size() && !isspace(s[j])) // test if s[j] is whitespace
			j++;
			// if we found some nonwhitespace chars
			if (i != j)
			{
				// substr: copy from s starting at i and taking j - i chars, copies [i,j)
				ret.push_back(s.substr(i,j-1));
				i = j;
			}
	}
	return ret;
}

int main()
{
	string s;
	// read and split each line of input
	while (getline(cin,s)) // getline returns ref to istream
	{
		vector<string> v = split(s);

		// write each word in v on separate line
		for (vector<string>::size_type i = 0; i != v.size(); ++i)
			cout <<v[i]<<endl;
	}
	return 0;
}

// library derived program that is used for comparison
int main()
{
	string s;
	while (cin >> s)
		cout << s << endl;
	return 0;
}
